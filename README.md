# Content Server ODBC Tester

The `odbc_tester` utility will time prepare, bind, execute and fetch and close
stages of a query execution against a Content Server database. A
`odbc_tester_#####.csv` file is created for each execution with the execution
times.

## Usage

```
USAGE:
    odbc_tester.exe --cycles <CYCLES> --database <DATABASE> --driver <DRIVER> --executions <EXECUTIONS> --password <PASSWORD> --server <SERVER> --sleep <SLEEP> --user <USER>

FLAGS:
    -h, --help       Prints help information
    -V, --version    Prints version information

OPTIONS:
    -C, --cycles <CYCLES>            Number of cycles to perform.
    -d, --database <DATABASE>        SQL Server database.
    -D, --driver <DRIVER>            SQL Server ODBC driver.
    -E, --executions <EXECUTIONS>    Number of SQL executions per cycle to perform.
    -P, --password <PASSWORD>        SQL Server user password.
    -S, --server <SERVER>            SQL Server host.
    -Z, --sleep <SLEEP>              Sleep time in seconds between cycle executions.
    -U, --user <USER>                SQL Server user.
```

## Execution

```
odbc_tester.exe -D "SQL Server" -S davidh-db.otxlab.net -d cs_22_1 -U cs_22_1 -P Livelink1! -C 2 -E 5 -Z 1
```

```
2022-02-16 15:48:29,633 INFO [odbc_tester] odbc_tester 1.1.0
2022-02-16 15:48:29,633 INFO [odbc_tester::odbc_tester] CSV data written to odbc_tester_1645044509.csv
2022-02-16 15:48:30,639 INFO [odbc_tester::odbc_tester] Cycle 1
2022-02-16 15:48:30,639 INFO [odbc_tester::odbc_tester] Connecting to Driver={SQL Server};Server=davidh-db.otxlab.net;Database=cs_22_1;Uid=cs_22_1;Pwd=xxxxxxxxxxxx
2022-02-16 15:48:30,675 WARN [odbc::result] State: 01000, Native error: 5701, Message: [Microsoft][ODBC SQL Server Driver][SQL Server]Changed database context to 'cs_22_1'.
2022-02-16 15:48:30,677 WARN [odbc::result] State: 01000, Native error: 5703, Message: [Microsoft][ODBC SQL Server Driver][SQL Server]Changed language setting to us_english.
2022-02-16 15:48:30,678 INFO [odbc_tester::odbc_tester] Connection time: 36860us
2022-02-16 15:48:30,678 INFO [odbc_tester::odbc_tester] Executing query: select * from KState where StateID=?
2022-02-16 15:48:30,679 INFO [odbc_tester::odbc_tester] Binding parameters: 2
2022-02-16 15:48:31,691 INFO [odbc_tester::odbc_tester] Query processing times: Prepare(3444us), Bind(1569us), Execute(3145us), Fetch(2386us), Close(1292us), Query(11841us))
2022-02-16 15:48:32,702 INFO [odbc_tester::odbc_tester] Query processing times: Prepare(2688us), Bind(1350us), Execute(2593us), Fetch(2608us), Close(1424us), Query(10667us))
2022-02-16 15:48:33,715 INFO [odbc_tester::odbc_tester] Query processing times: Prepare(2683us), Bind(1384us), Execute(3176us), Fetch(2728us), Close(1356us), Query(11331us))
2022-02-16 15:48:34,727 INFO [odbc_tester::odbc_tester] Query processing times: Prepare(4056us), Bind(1184us), Execute(2887us), Fetch(2603us), Close(1452us), Query(12186us))
2022-02-16 15:48:35,739 INFO [odbc_tester::odbc_tester] Query processing times: Prepare(2852us), Bind(1182us), Execute(2615us), Fetch(2312us), Close(1346us), Query(10311us))
2022-02-16 15:48:36,743 INFO [odbc_tester::odbc_tester] Cycle 2
2022-02-16 15:48:36,743 INFO [odbc_tester::odbc_tester] Connecting to Driver={SQL Server};Server=davidh-db.otxlab.net;Database=cs_22_1;Uid=cs_22_1;Pwd=xxxxxxxxxxxx
2022-02-16 15:48:36,756 WARN [odbc::result] State: 01000, Native error: 5701, Message: [Microsoft][ODBC SQL Server Driver][SQL Server]Changed database context to 'cs_22_1'.
2022-02-16 15:48:36,757 WARN [odbc::result] State: 01000, Native error: 5703, Message: [Microsoft][ODBC SQL Server Driver][SQL Server]Changed language setting to us_english.
2022-02-16 15:48:36,758 INFO [odbc_tester::odbc_tester] Connection time: 14772us
2022-02-16 15:48:36,759 INFO [odbc_tester::odbc_tester] Executing query: select * from KState where StateID=?
2022-02-16 15:48:36,759 INFO [odbc_tester::odbc_tester] Binding parameters: 2
2022-02-16 15:48:37,770 INFO [odbc_tester::odbc_tester] Query processing times: Prepare(3030us), Bind(1220us), Execute(2679us), Fetch(2385us), Close(1338us), Query(10657us))
2022-02-16 15:48:38,782 INFO [odbc_tester::odbc_tester] Query processing times: Prepare(3831us), Bind(1299us), Execute(3276us), Fetch(2195us), Close(1253us), Query(11859us))
2022-02-16 15:48:39,792 INFO [odbc_tester::odbc_tester] Query processing times: Prepare(2669us), Bind(1089us), Execute(2586us), Fetch(2196us), Close(1199us), Query(9743us))
2022-02-16 15:48:40,804 INFO [odbc_tester::odbc_tester] Query processing times: Prepare(2794us), Bind(1631us), Execute(2642us), Fetch(2336us), Close(1317us), Query(10725us))
2022-02-16 15:48:41,816 INFO [odbc_tester::odbc_tester] Query processing times: Prepare(2775us), Bind(1755us), Execute(2794us), Fetch(2256us), Close(1161us), Query(10744us))
```

## Sample CSV

```
DateTime,Cycle,Execution,Prepare(us),Bind(us),Execute(us),Fetch(us),Close(us),Query(us),Driver,Server,Database,User,Statement
2022-02-16 15:48:31.691,1,1,3444,1569,3145,2386,1292,11841,SQL Server,davidh-db.otxlab.net,cs_22_1,cs_22_1,select * from KState where StateID=?
2022-02-16 15:48:32.702,1,2,2688,1350,2593,2608,1424,10667,SQL Server,davidh-db.otxlab.net,cs_22_1,cs_22_1,select * from KState where StateID=?
2022-02-16 15:48:33.715,1,3,2683,1384,3176,2728,1356,11331,SQL Server,davidh-db.otxlab.net,cs_22_1,cs_22_1,select * from KState where StateID=?
2022-02-16 15:48:34.728,1,4,4056,1184,2887,2603,1452,12186,SQL Server,davidh-db.otxlab.net,cs_22_1,cs_22_1,select * from KState where StateID=?
2022-02-16 15:48:35.739,1,5,2852,1182,2615,2312,1346,10311,SQL Server,davidh-db.otxlab.net,cs_22_1,cs_22_1,select * from KState where StateID=?
2022-02-16 15:48:37.770,2,1,3030,1220,2679,2385,1338,10657,SQL Server,davidh-db.otxlab.net,cs_22_1,cs_22_1,select * from KState where StateID=?
2022-02-16 15:48:38.782,2,2,3831,1299,3276,2195,1255,11859,SQL Server,davidh-db.otxlab.net,cs_22_1,cs_22_1,select * from KState where StateID=?
2022-02-16 15:48:39.792,2,3,2669,1089,2586,2196,1199,9743,SQL Server,davidh-db.otxlab.net,cs_22_1,cs_22_1,select * from KState where StateID=?
2022-02-16 15:48:40.804,2,4,2794,1631,2642,2336,1317,10725,SQL Server,davidh-db.otxlab.net,cs_22_1,cs_22_1,select * from KState where StateID=?
2022-02-16 15:48:41.816,2,5,2775,1755,2794,2256,1161,10744,SQL Server,davidh-db.otxlab.net,cs_22_1,cs_22_1,select * from KState where StateID=?
```

## Building

1. Install Rust 1.59.0.

2. Clone this repository.

3. Run `cargo build --release` to generate a release binary which is located in `./target/release/otdb_tester.exe`. 
