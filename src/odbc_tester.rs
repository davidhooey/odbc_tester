extern crate chrono;
extern crate csv;

use chrono::Local;
use csv::WriterBuilder;
use log;
use odbc::*;
use std::time::{Instant, SystemTime, UNIX_EPOCH};
use std::{thread, time};

pub fn test(driver :String, server :String, database :String, user :String, password :String, cycles :u64, executions :u64, sleep :u64) {

    let start = SystemTime::now();
    let csv_file = format!("odbc_tester_{}.csv", start.duration_since(UNIX_EPOCH).unwrap().as_secs());
    log::info!("CSV data written to {}", csv_file);

    // Write CSV header
    let mut wtr = WriterBuilder::new().double_quote(true).from_path(csv_file).unwrap();
    wtr.write_record(&[
        "DateTime", 
        "Cycle", 
        "Execution", 
        "Prepare(us)", 
        "Bind(us)", 
        "Execute(us)", 
        "Fetch(us)", 
        "Close(us)", 
        "Query(us)", 
        "Driver", 
        "Server", 
        "Database", 
        "User", 
        "Statement"
    ]).unwrap();
    wtr.flush().unwrap();

    let env = create_environment_v3().unwrap();

    // Driver={SQL Server};Server=serverName;Database=databaseName;Uid=userName;Pwd=password
    let connection_string: String = format!("Driver={{{}}};Server={};Database={};Uid={};Pwd={}", driver, server, database, user, password);
    let connection_string_no_pwd: String = format!("Driver={{{}}};Server={};Database={};Uid={};Pwd=xxxxxxxxxxxx", driver, server, database, user);

    for cycle_count in 1..=cycles {
        let sleep_time = time::Duration::from_secs(sleep);
        thread::sleep(sleep_time);

        log::info!("Cycle {}", cycle_count);

        log::info!("Connecting to {}", connection_string_no_pwd);

        let connection_start = Instant::now();
        let conn = env.connect_with_connection_string(&connection_string).unwrap();
        let connection_duration = connection_start.elapsed();
        log::info!("Connection time: {}us", connection_duration.as_micros());

        // select IniSection, IniKeyword, IniValue from KIni where IniSection = 'Livelink.VersionInfo'
        // let stmt_string = "select IniValue from KIni where IniSection = ?";
        let stmt_string = "select * from KState where StateID=?";
        let bind = 2;

        log::info!("Executing query: {}", stmt_string);
        log::info!("Binding parameters: {}", bind);

        for execution_count in 1..=executions {
            let sleep_time = time::Duration::from_secs(1);
            thread::sleep(sleep_time);

            let mut execute_duration = time::Duration::from_secs(0);
            let mut fetch_duration = time::Duration::from_secs(0);
            let mut close_start = Instant::now();

            let query_start = Instant::now();

            let prepare_start = Instant::now();
            let stmt = Statement::with_parent(&conn).unwrap().prepare(stmt_string).unwrap();
            let prepare_duration = prepare_start.elapsed();

            let bind_start = Instant::now();
            let stmt = stmt.bind_parameter::<i32>(1, &bind).unwrap();
            let bind_duration = bind_start.elapsed();

            let execute_start = Instant::now();
            match stmt.execute() {
                Ok(Data(mut stmt)) => {
                    execute_duration = execute_start.elapsed();

                    let fetch_start = Instant::now();
                    if let Some(mut cursor) = stmt.fetch().unwrap() {
                        match cursor.get_data::<String>(1).unwrap() {
                            Some(_val) => {
                                fetch_duration = fetch_start.elapsed();
                                close_start = Instant::now();
                            }
                            None => log::error!("Failed to get data."),
                        }
                    } else {
                        log::error!("Fetch failed.");
                    }
                },
                Ok(NoData(_stmt)) => {
                    log::error!("No data");
                },
                Err(e) => {
                    log::error!("Error executing statement: {:?}", e);
                }
            };

            let close_duration = close_start.elapsed();
            let query_duration = query_start.elapsed();

            log::info!(
                "Query processing times: Prepare({}us), Bind({}us), Execute({}us), Fetch({}us), Close({}us), Query({}us))",
                prepare_duration.as_micros(), bind_duration.as_micros(), execute_duration.as_micros(), fetch_duration.as_micros(), close_duration.as_micros(), query_duration.as_micros()
            );

            wtr.write_record(&[
                &format!("{}", Local::now().format("%Y-%m-%d %H:%M:%S%.3f")), 
                &format!("{}", cycle_count), 
                &format!("{}", execution_count), 
                &format!("{}", prepare_duration.as_micros()), 
                &format!("{}", bind_duration.as_micros()), 
                &format!("{}", execute_duration.as_micros()),
                &format!("{}", fetch_duration.as_micros()), 
                &format!("{}", close_duration.as_micros()),
                &format!("{}", query_duration.as_micros()),
                &format!("{}", driver),
                &format!("{}", server),
                &format!("{}", database),
                &format!("{}", user),
                &format!("{}", stmt_string)
            ]).unwrap();
            wtr.flush().unwrap();
        }
    }
}
