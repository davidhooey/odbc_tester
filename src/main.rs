extern crate log;
extern crate simple_logger;

mod odbc_tester;

use log::Level;
use std::env;

fn main() {
    simple_logger::init_with_level(Level::Info).unwrap();

    let matches = clap::App::new("odbc_tester")
        .setting(clap::AppSettings::ArgRequiredElseHelp)
        .version(env!("CARGO_PKG_VERSION"))
        .author("David Hooey <davidh@opentext.com>")
        .about("Test an ODBC connection.")
        .arg(
            clap::Arg::with_name("driver")
                .short("-D")
                .long("driver")
                .value_name("DRIVER")
                .help("SQL Server ODBC driver.")
                .takes_value(true)
                .required(true)
        )
        .arg(
            clap::Arg::with_name("server")
                .short("-S")
                .long("server")
                .value_name("SERVER")
                .help("SQL Server host.")
                .takes_value(true)
                .required(true)
        )
        .arg(
            clap::Arg::with_name("database")
                .short("-d")
                .long("database")
                .value_name("DATABASE")
                .help("SQL Server database.")
                .takes_value(true)
                .required(true)
        )
        .arg(
            clap::Arg::with_name("user")
                .short("-U")
                .long("user")
                .value_name("USER")
                .help("SQL Server user.")
                .takes_value(true)
                .required(true)
        )
        .arg(
            clap::Arg::with_name("password")
                .short("-P")
                .long("password")
                .value_name("PASSWORD")
                .help("SQL Server user password.")
                .takes_value(true)
                .required(true)
        )
        .arg(
            clap::Arg::with_name("cycles")
                .short("-C")
                .long("cycles")
                .value_name("CYCLES")
                .help("Number of cycles to perform.")
                .takes_value(true)
                .required(true)
        )
        .arg(
            clap::Arg::with_name("executions")
                .short("-E")
                .long("executions")
                .value_name("EXECUTIONS")
                .help("Number of SQL executions per cycle to perform.")
                .takes_value(true)
                .required(true)
        )
        .arg(
            clap::Arg::with_name("sleep")
                .short("-Z")
                .long("sleep")
                .value_name("SLEEP")
                .help("Sleep time in seconds between cycle executions.")
                .takes_value(true)
                .required(true)
        )
        .get_matches();

    log::info!("odbc_tester {}", env!("CARGO_PKG_VERSION"));

    let driver: String = matches.value_of("driver").unwrap().to_string();
    let server: String = matches.value_of("server").unwrap().to_string();
    let database: String = matches.value_of("database").unwrap().to_string();
    let user: String = matches.value_of("user").unwrap().to_string();
    let password: String = matches.value_of("password").unwrap().to_string();
    let cycles: u64 = matches.value_of("cycles").unwrap().to_string().parse::<u64>().unwrap();
    let executions: u64 = matches.value_of("executions").unwrap().to_string().parse::<u64>().unwrap();
    let sleep: u64 = matches.value_of("sleep").unwrap().to_string().parse::<u64>().unwrap();

    odbc_tester::test(driver, server, database, user, password, cycles, executions, sleep);
}
